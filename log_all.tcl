
# set dir_id "80350001"

# log $dir_id/xspec.log
log xspec.log

show par

error 2 3 4 5

freeze 2 3 4

# save all $dir_id/base.xcm
save all base.xcm


#=========powerlaw flux

add 2 cflux
/*

newpar 2 0.5 -1
newpar 3 10.0 -1

fit

error 4
error 8

# save all $dir_id/flux_0.5-10.0.xcm
save all flux_0.5-10.0.xcm

#=========model flux 1

set min_energy 0.5
set max_energy 2.0

@$dir_id/base

add 1 cflux
/*

newpar 1 $min_energy -1
newpar 2 $max_energy -1

fit

error 3 8

# save all $dir_id/flux_$min_energy-$max_energy.xcm


#=========model flux 2

set min_energy 2.0
set max_energy 10.0

@$dir_id/base

add 1 cflux
/*

newpar 1 $min_energy -1
newpar 2 $max_energy -1

fit

error 3 8

# save all $dir_id/flux_$min_energy-$max_energy.xcm
save all flux_$min_energy-$max_energy.xcm


log none