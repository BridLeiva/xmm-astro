#!/usr/bin/env python3

import os
import sys
import time

import subprocess as subproc

from concurrent.futures import ProcessPoolExecutor, as_completed

ignore_dirs = ['abstracts', '_0206450301']
obs_dir = '/home/sophos/devel/xmm-astro/xmm-obs'
project_root = '/home/sophos/devel/xmm-astro'


def main():
    for d in os.listdir("./xmm-obs"):
        if d in ignore_dirs: continue

        if (obs_id := get_arg('obs')) and d != obs_id:
            continue

        process_obs(d)    

def async_main():
    with ProcessPoolExecutor() as exe:
        futrs = []

        for d in os.listdir("./xmm-obs"):
            if d in ignore_dirs: continue

            if (obs_id := get_arg('obs')) and d != obs_id:
                continue

            futrs.append( exe.submit(process_obs, d) )

        for ftr in as_completed(futrs):
            try:
                ftr.result()

            except Exception as e:
                print(e)
                continue


def process_obs(obs_id):
    functs = [
        "startup"
        , "filter_flaring_particle_bkg"
        , "extract_light_curves"
        , "extract_spectrum"
    ]

    start_idx = functs.index( start ) if (start := get_arg('from')) else 0
    exact_idx = functs.index( exact ) if (exact := get_arg('step')) else -1

    for idx, name in enumerate(functs):
        if idx < start_idx: continue

        if exact_idx >= 0 \
        and idx != exact_idx:
            continue

        eval(f"{name}('{obs_id}')")

def startup(obs_id):
    obs_root = f"{obs_dir}/{obs_id}"
    os.environ['SAS_ODF'] = f"{obs_root}/ODF"

    cif_name = f"{obs_id}_ccf.cif"

    subproc.run(["cifbuild", f"calindexset={cif_name}"])
    os.environ['SAS_CCF'] = f"{project_root}/{cif_name}"

    subproc.run(["odfingest"])

    for f in os.listdir(project_root):
    	if f"{obs_id}_SCX00000SUM.SAS" not in f:
    		continue

    	os.environ['SAS_ODF'] = f"{project_root}/{f}"
    	break
    
    if '--debug' in sys.argv:
        print(os.environ.get('SAS_ODF', 'NOTHING'))

    # create EPIC event list
    
    comm = "emproc" if data_type in ('MOS1', 'MOS2') else "epproc"

    subproc.run([comm])

def filter_flaring_particle_bkg(obs_id):
    evts_file = events_file(obs_id)

    subproc.run(rateset_comm(obs_id, evts_file), shell= True)
    subproc.run(gtiset_comm(obs_id, evts_file), shell= True)
    subproc.run(filteredset_comm(obs_id, evts_file), shell= True)

def rateset_comm(obs_id, evts_file):
    rate_file = f"{obs_id}_{data_type}_rate.fits"

    expression = "#XMMEA_EM && (PI>10000) && (PATTERN==0)" \
        if data_type in ('MOS1', 'MOS2') \
        else "#XMMEA_EP && (PI>10000&&PI<12000) && (PATTERN==0)"

    rateset_comm_str = (
        f"evselect table={evts_file} withrateset=Y rateset={rate_file} "
        " maketimecolumn=Y timebinsize=10 makeratecolumn=Y "
        f" expression='{expression}' "
        )

    return rateset_comm_str

def gtiset_comm(obs_id, evts_file):
    rate_file = f"{obs_id}_{data_type}_rate.fits"
    gti_file = f"{obs_id}_{data_type}_gti.fits"

    expression = "RATE<=0.35" \
        if data_type in ('MOS1', 'MOS2') \
        else "RATE<=0.4"

    gtiset_comm_str = (
        f"tabgtigen table={rate_file} gtiset={gti_file}"
        f" expression='{expression}' "
        )

    return gtiset_comm_str

def filteredset_comm(obs_id, evts_file):
    gti_file = f"{obs_id}_{data_type}_gti.fits"
    filtered_file = f"{obs_id}_{data_type}_clean.fits"

    expression = f"#XMMEA_EM && gti({gti_file},TIME) && (PI>150)" \
        if data_type in ('MOS1', 'MOS2') \
        else f"XMMEA_EP && gti({gti_file},TIME) && (PI>150)"

    filteredset_comm_str = (
        f"evselect table={evts_file} filteredset={filtered_file}"
        " withfilteredset=Y destruct=Y keepfilteroutput=T"
        f" expression='{expression}' "
        )

    return filteredset_comm_str


def extract_light_curves(obs_id):
    evts_file = f"{obs_id}_{data_type}_clean.fits"

    # extract one image
    img_extract_comm = (
        f"evselect table={evts_file} imageset={obs_id}_image.img "
        " imagebinning=binSize withimageset=yes"
        " xcolumn=X ycolumn=Y ximagebinsize=80 yimagebinsize=80"
        )

    subproc.run(img_extract_comm, shell= True)

    # wait until source and background are defined
    # see the imgage with imgdisplay withimagefile=true imagefile=image.img
    wait_time = 30
    while not (regs := regions(obs_id)):
        print(f'waiting for {wait_time} s...')
        time.sleep(wait_time)

        wait_time = 2 * wait_time

    src, bkg = regs.split('|')

    for energy in ('LOW', 'HIGH'):
        rng, rng_str = ranges(energy)

        lc_source_file = f"{obs_id}_{data_type}_lightcurve_source_raw_{rng_str}.lc"
        lc_background_file = f"{obs_id}_{data_type}_lightcurve_background_raw_{rng_str}.lc"
        corr_file = f"{obs_id}_{data_type}_lccorr_{rng_str}.lc"

        expression = "#XMMEA_EM && (PATTERN<=12)" \
            if data_type in ('MOS1', 'MOS2') \
            else "#XMMEA_EP && (PATTERN<=4)" 

        lc_src_comm = (
            f"evselect table={evts_file} energycolumn=PI"
            f" expression='{expression} && ((X,Y) IN circle({src})) && (PI in [{rng}])'"
            f" withrateset=yes rateset='{lc_source_file}'"
            " timebinsize=10 maketimecolumn=yes makeratecolumn=yes"
            )

        lc_bkg_comm = (
            f"evselect table={evts_file} energycolumn=PI"
            f" expression='{expression} && ((X,Y) IN circle({bkg})) && (PI in [{rng}])'"
            f" withrateset=yes rateset='{lc_background_file}'"
            " timebinsize=10 maketimecolumn=yes makeratecolumn=yes"
            )

        lc_corr_comm = (
            f"epiclccorr srctslist={lc_source_file}"
            f" eventlist={evts_file} outset={corr_file} bkgtslist={lc_background_file}"
            " withbkgset=yes applyabsolutecorrections=yes"
            )

        subproc.run(lc_src_comm, shell= True)
        subproc.run(lc_bkg_comm, shell= True)
        subproc.run(lc_corr_comm, shell= True)

def ranges(energy):
    rngs = {
        'ALL': ('300:10000', '.3-10')
        , 'LOW': ('300:2000', '.3-2')
        , 'HIGH': ('2000:10000', '2-10')
    }

    return rngs[energy]

def extract_spectrum(obs_id):
    # evts_file = events_file(obs_id)
    evts_file = f"{obs_id}_{data_type}_clean.fits"

    # wait until source and background are defined
    wait_time = 30
    while not (regs := regions(obs_id)):
        print(f'waiting for {wait_time} s...')
        time.sleep(wait_time)

        wait_time = 2 * wait_time

    src, bkg = regs.split('|')

    for energy in ('ALL',):

        rng, rng_str = ranges(energy)

        spectrum_source_file = f"{obs_id}_{data_type}_spectrum_source_{rng_str}.ds"
        spectrum_background_file = f"{obs_id}_{data_type}_spectrum_background_{rng_str}.ds"
        
        expression = "#XMMEA_EM && (PATTERN<=12)" \
            if data_type in ('MOS1', 'MOS2') \
            else "(FLAG==0) && (PATTERN<=4)"

        max_channel = "11999" if data_type in ('MOS1', 'MOS2') else "20479"

        spectrum_source_comm = (
            f"evselect table={evts_file} withspectrumset=yes spectrumset={spectrum_source_file} "
            " energycolumn=PI spectralbinsize=5 withspecranges=yes "
            f" specchannelmin=0 specchannelmax={max_channel} "
            f" expression='{expression} && ((X,Y) IN circle({src}))' "
            )

        spectrum_background_comm = (
            f"evselect table={evts_file} withspectrumset=yes spectrumset={spectrum_background_file} "
            " energycolumn=PI spectralbinsize=5 withspecranges=yes "
            f" specchannelmin=0 specchannelmax={max_channel} "
            f" expression='{expression} && ((X,Y) IN circle({bkg}))' "
            )

        # extract source an bkg for spectrum
        subproc.run(spectrum_source_comm, shell= True)
        subproc.run(spectrum_background_comm, shell= True)

        # calculate area of source
        subproc.run(
            f"backscale spectrumset={spectrum_source_file} badpixlocation={evts_file}"
            , shell= True
            )

        subproc.run(
            f"backscale spectrumset={spectrum_background_file} badpixlocation={evts_file}"
            , shell= True
            )

        # generate redistribution matrix
        rmfgen_comm = (
            f"rmfgen spectrumset={spectrum_source_file} rmfset={obs_id}_{data_type}.rmf "
            " withenergybins=yes energymin=0.1 energymax=12.0 nenergybins=2400 "
            )
        subproc.run(rmfgen_comm, shell= True)

        # generate ancilliary file
        arf_comm = (
            f"arfgen spectrumset={spectrum_source_file} arfset={obs_id}_{data_type}.arf "
            f" withrmfset=yes rmfset={obs_id}_{data_type}.rmf "
            # f" badpixlocation={evts_file} detmaptype=psf "
            )
        subproc.run(arf_comm, shell= True)

        # rebin and link
        rebin_comm = (
            f"specgroup spectrumset={spectrum_source_file} "
            f" mincounts=20 oversample=3 rmfset={obs_id}_{data_type}.rmf "
            f" arfset={obs_id}_{data_type}.arf backgndset={spectrum_background_file} "
            f" groupedset={obs_id}_{data_type}_spectrum_grp.fits"
            )
        subproc.run(rebin_comm, shell= True)


def events_file(obs_id):

    data_type_str = f"_E{data_type}_"

    for f in os.listdir(project_root):

        if obs_id not in f \
            or '_ImagingEvts.ds' not in f \
            or data_type_str not in f:
            
            continue

        print(f)
        return f

def regions(obs_id):
    with open('regions.txt', 'r') as file:
        for line in file.readlines():
            obs, val = line.split('=')

            if obs.strip() != obs_id:
                continue

            return val

    return ""

def get_arg(arg_name):
    try:
        idx = sys.argv.index(f"--{arg_name}")
        return sys.argv[idx +1]

    except ValueError as e:
        if '--debug' in sys.argv:
            print(f'NOTICE: {arg_name} not found...')

        return ''

    except IndexError as e:
        if '--debug' in sys.argv:
            print(f"{arg_name} is not a flag...")

        return ''

data_type = data.upper() \
    if (data := get_arg('data')) else "PN"

if __name__ == '__main__':
    async_main() if '--async' in sys.argv else main()
    