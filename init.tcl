# This script gives an example of how one might use the power of tcl's
# scripting language in an XSPEC session.  In this example, XSPEC loops
# thru 3 data files (file1, file2 and file3) and fits them each to the
# same model `wabs(po+ga)'.  After the fit the values of 4 parameters
# for each data set is saved to a file.

# Return TCL results for XSPEC commands.
set xs_return_result 1

# set dir_id "80350001"

# Keep going until fit converges.
query yes

# Open the file to put the results in.
# set fileid [open $dir_id/fit_result.dat w]

# Get the file.
# data $dir_id/linked_rbn.pha
data MOS_spectrum_grp.fits

ignore bad
ignore **-0.5 10.0-**

energies 0.01 100.0 1000 log
statistic cstat

# Set up the model.
model tbabs(powerlaw+bbody)
/*

newpar 1 5.14E-2 -1
newpar 4 0.1

renorm
fit

steppar 4 0.05 0.2

cpd /xs
setplot energy
plot data ratio

show par